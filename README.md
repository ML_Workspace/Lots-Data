Lots(Lost) Data
===

This API is for storing **any kinds of** data, no matter they are numbers, words, decimals, paragraphs, even if **files**.

## Base URL

URL of the PHP root file.

## GET

### Check the username whether it is duplicated or not

#### Sent

	HEADER:
		Dup: username
		Username: [STRING]

- Username
	- The username that you want to check is duplicated or not.

#### Received

	{
		"result": [BOOLEAN]
	}

- result
	- 0: Not duplicated.
	- 1: Duplicated.

### Check the email whether it is duplicated or not

#### Sent

	HEADER:
		Dup: email
		Email: [STRING]

- Email
	- The email that you want to check is duplicated or not.

#### Received

	{
		"result": [BOOLEAN]
	}

- result
	- 0: Not duplicated.
	- 1: Duplicated.

### Get token

#### Sent

	HEADER:
		Auth: [STRING]
		Pswd: [STRING]

- Auth
	- Your username.
- Pswd
	- Your password.

#### Received

	{
		"token": [STRING]
	}

- token
	- Whenever your account is signed in, your token is set to be randomized.  
	Token will be refresh in different session for secure reason.  
	After session times out (which is `30 minutes`), your account will automatically sign out, which means your token will be invalid due to session expired.

### Get UID

#### Sent

	HEADER:
		Token: [STRING]
		Get: uid

- Token
	- Your valid token.

#### Received

	{
		"uid": [STRING]
	}

- uid
	- Your UID.

### Get email

#### Sent

	HEADER:
		Token: [STRING]
		Get: email

- Token
	- Your valid token.

#### Received

	{
		"email": [STRING]
	}

- email
	- Your email.

### Get sensors

#### Sent

	HEADER:
		Token: [STRING]
		Get: sensors

- Token
	- Your valid token.

#### Received

	[
		{
			"uid": [STRING]
		},
		{
			"uid": [STRING]
		}
		...
	]

- uid
	- The UID(s) of your sensor(s).

### Get sensor info

#### Sent

	HEADER:
		Token: [STRING]
		Get: info
		Uid: [STRING]

- Token
	- Your valid token.
- Uid
	- The UID of your sensor.

#### Received

	{
		"uid": [STRING],
		"owner": [STRING],
		"name": [STRING],
		"description": [STRING],
		"type": [INTEGER],
		"users": [STRING],
		"tables": [STRING]
	}

- uid
	- The UID of this sensor.
- owner
	- The owner(creator) of this sensor.
- name
	- The name of this sensor.
- description
	- The description of this sensor.
- type
	- The type of data this sensor(s) stored.
- users
	- All user UIDs, each has the permission to add and getting values.
- tables
	- All table UIDs, each include current sensor.

### Get sensor data

#### Sent

	HEADER:
		Token: [STRING]
		Get: sensor
		Uid: [STRING]

- Token
	- Your valid token.
- Uid
	- The UID of your sensor.

#### Received

	[
		{
			"createTime": [INTEGER],
			"val": [INTEGER | FLOAT | DECIMAL | STRING]
		},
		{
			"createTime": [INTEGER],
			"val": [INTEGER | FLOAT | DECIMAL | STRING]
		}
		...
	]

- createTime
	- The created time of this data.
- val
	- The data.

### Get tables

#### Sent

	HEADER:
		Token: [STRING]
		Get: tables

- Token
	- Your valid token.

#### Received

	[
		{
			"uid": [STRING]
		},
		{
			"uid": [STRING]
		}
		...
	]

- uid
	- The UID(s) of your table(s).

### Get table info

#### Sent

	HEADER:
		Token: [STRING]
		Get: info
		Uid: [STRING]

- Token
	- Your valid token.
- Uid
	- The UID of your table.

#### Received

	{
		"uid": [STRING],
		"owner": [STRING],
		"name": [STRING],
		"description": [STRING],
		"users": [STRING]
	}

- uid
	- The UID of this table.
- owner
	- The owner(creator) of this table.
- name
	- The name of this table.
- description
	- The description of this table.
- users
	- All user UID, each has the permission to get values.

### Get table data

#### Sent

	HEADER:
		Token: [STRING]
		Get: table
		Uid: [STRING]

- Token
	- Your valid token.
- Uid
	- The UID of your table.

#### Received

	[
		{
			"uid": [STRING],
			"layout": [INTEGER],
			"name": [STRING],
			"description": [STRING],
			"type": [INTEGER],
			"data": [
				{
					"createTime": [INTEGER],
					"val": [INTEGER | FLOAT | DECIMAL | STRING]
				},
				{
					"createTime": [INTEGER],
					"val": [INTEGER | FLOAT | DECIMAL | STRING]
				}
				...
			]
		},
		{
			"uid": [STRING],
			"layout": [INTEGER],
			"name": [STRING],
			"description": [STRING],
			"type": [INTEGER],
			"data": [
				{
					"createTime": [INTEGER],
					"val": [INTEGER | FLOAT | DECIMAL | STRING]
				},
				{
					"createTime": [INTEGER],
					"val": [INTEGER | FLOAT | DECIMAL | STRING]
				}
				...
			]
		}
		...
	]

- uid
	- The UID of the sensor(s).
- layout
	- The layout of the sensor(s).
- name
	- The name of the sensor(s).
- description
	- The description of the sensor(s).
- type
	- The type of data that sensor(s) stored.
- data
	- All data of the sensor(s).
- createTime
	- The created time of this data.
- val
	- The data.

## POST

### Create user

#### Sent

	BODY:
		cate: user
		username: [STRING]
		pswd: [STRING]
		email: [STRING]

- username
	- The username you want to use.
- pswd
	- The password you want to use.
- email
	- The email you want to use.

#### Received

	{
		"uid": [STRING]
	}

- uid
	- Your UID.

### Create sensor

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: sensor
		name: [STRING]
		desc: [STRING]
		type: [INTEGER]

- Token
	- Your valid token.
- name
	- The name of your sensor.
- desc
	- The description of your sensor.
- type
	- The type of data your sensor stored.
		- Signed numeric
			- 01 => `TINYINT`  
			`-128 to 127`
			- 02 => `SMALLINT`  
			`-32,768 to 32,767`
			- 03 => `MEDIUMINT`  
			`-8,388,608 to 8,388,607`
			- 04 => `INT`  
			`-2,147,483,648 to 2,147,483,647`
			- 05 => `BIGINT`  
			`-9,223,372,036,854,775,808 to 9,223,372,036,854,775,807`
			- 06 => `DECIMAL`  
			`digits is 65, decimals is 30`
			- 07 => `FLOAT`  
			`-3.402823466E+38 to -1.175494351E-38, 0, and 1.175494351E-38 to 3.402823466E+38`
			- 08 => `DOUBLE`  
			`-1.7976931348623157E+308 to -2.2250738585072014E-308, 0, and 2.2250738585072014E-308 to 1.7976931348623157E+308`
		- Unsigned numeric
			- 11 => `TINYINT`  
			`0 to 255`
			- 12 => `SMALLINT`  
			`0 to 65,535`
			- 13 => `MEDIUMINT`  
			`0 to 16,777,215`
			- 14 => `INT`  
			`0 to 4,294,967,295`
			- 15 => `BIGINT`  
			`0 to 18,446,744,073,709,551,615`
			- 16 => `DECIMAL`  
			`digits is 65, decimals is 30`
			- 17 => `FLOAT`  
			`0 and 1.175494351E-38 to 3.402823466E+38`
			- 18 => `DOUBLE`  
			`0 and 2.2250738585072014E-308 to 1.7976931348623157E+308`
		- String
			- 21 => `TINYTEXT`  
			`255 (2^8 - 1) characters`
			- 22 => `TEXT`  
			`65,535 (2^16 - 1) characters`
			- 23 => `MEDIUMTEXT`  
			`16,777,215 (2^24 - 1) characters`
			- 24 => `LONGTEXT`  
			`4,294,967,295 or 4GiB (2^32 - 1) characters`

#### Received

	{
		"uid": [STRING],
		"type": [STRING]
	}

- uid
	- The UID of this sensor.
- type
	- The type of data this sensor stored.

### Create table

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: table
		name: [STRING]
		desc: [STRING]

- Token
	- Your valid token.
- name
	- The name of your table.
- desc
	- The description of your table.

#### Received

	{
		"uid": [STRING]
	}

- uid
	- The UID of this table.

## PUT

### Update user's info

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: user
		username: [STRING]
		email: [STRING]
		pswd: [STRING]

- Token
	- Your valid token.
- username (optional)
	- Your new username.
- email (optional)
	- Your new email.
- pswd (optional)
	- Your new password.

#### Received

	N/A

### Update sensor name

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: sensor
		uid: [STRING]
		name: [STRING]

- Token
	- Your valid token.
- uid
	- The UID of the sensor you want to update.
- name
	- The new name of the sensor.

#### Received

	N/A

### Update sensor description

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: sensor
		uid: [STRING]
		desc: [STRING]

- Token
	- Your valid token.
- uid
	- The UID of the sensor you want to update.
- desc
	- The new description of the sensor.

#### Received

	N/A

### Add sensor value

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: sensor
		uid: [STRING]
		val: [INTEGER | FLOAT | DECIMAL | STRING]

- Token
	- Your valid token.
- uid
	- The UID of the sensor you want to update.
- val
	- Your data.

#### Received

	N/A

### Add sensor users

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: sensor
		uid: [STRING]
		user: [STRING]

- Token
	- Your valid token.
- uid
	- The UID of the sensor you want to update.
- user
	- The UID of the user which you want to share the sensor with.

#### Received

	N/A

### Update table name

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: table
		uid: [STRING]
		name: [STRING]
- uid
	- The UID of the table you want to update.
- Token
	- Your valid token.
- name
	- The new name of the table.

#### Received

	N/A

### Update table description

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: table
		uid: [STRING]
		desc: [STRING]

- Token
	- Your valid token.
- uid
	- The UID of the table you want to update.
- desc
	- The new description of the table.

#### Received

	N/A

### Update table sensor

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: table
		uid: [STRING]
		sensor: [STRING]

- Token
	- Your valid token.
- uid
	- The UID of the table you want to update.
- sensor
	- The UID of the sensor you want to add into the table.

#### Received

	N/A

### Update table users

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: table
		uid: [STRING]
		user: [STRING]

- Token
	- Your valid token.
- uid
	- The UID of the table you want to update.
- user
	- The UID of the user which you want to share this table with.

#### Received

	N/A

## DELETE

### Delete token

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: token

- Token
	- Your valid token.

#### Received

	N/A

### Delete user from sensor

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: sensor
		remove: [STRING]
		from: [STRING]

- Token
	- Your valid token.
- remove
	- The UID of the user you want to remove.
- from
	- The UID of the sensor you want to remove from.

#### Received

	N/A

### Delete sensor

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: sensor
		uid: [STRING]

- Token
	- Your valid token.
- uid
	- The UID of the sensor you want to delete.

#### Received

	N/A

### Delete sensor of table

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: table
		uid: [STRING]
		from: [STRING]

- Token
	- Your valid token.
- uid
	- The UID of the sensor you want to remove.
- from
	- The UID of the table you want to remove from.

#### Received

	N/A

### Delete user from table

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: table
		remove: [STRING]
		from: [STRING]

- Token
	- Your valid token.
- remove
	- The UID of the user you want to remove.
- from
	- The UID of the table you want to remove from.

#### Received

	N/A

### Delete table

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: table
		uid: [STRING]

- Token
	- Your valid token.
- uid
	- The UID of the table you want to delete.

#### Received

	N/A

### Delete user

#### Sent

	HEADER:
		Token: [STRING]
	BODY:
		cate: user
		uid: [STRING]
		pswd: [STRING]

- Token
	- Your valid token.
- uid
	- Your UID.
- pswd
	- Your password.

#### Received

	N/A
