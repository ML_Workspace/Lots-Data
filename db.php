<?php
	//Database connection information, default table name is 'lotsData'
	include 'lib/db.php';	//Remove this line
	//$db = new mysqli('database location', 'username', 'password', 'database name');

	include 'lib/http.php';
	include 'lib/mysqli.php';
	include 'lib/safe.php';
	include 'lib/functions.php';
	if ($db->connect_errno) {
		http(500);
		die($db->connect_errno . ' ' . $db->connect_error);
		exit;
	}
	if (!$db->set_charset('utf8mb4')) mysqli_echo_error();
	$_HEADER = getHeader();
	$_HTTP = getBody();
	$tknuid = verify(isset($_HEADER['token'])?safe($_HEADER['token']):'');
	switch ($_SERVER['REQUEST_METHOD']) {
		case 'GET':
			if (isset($_HEADER['dup'])) {
				if (!isset($_HEADER[ucwords(strtolower($_HEADER['dup']))]) || $_HEADER['dup'] == 'uid' || $_HEADER['dup'] == 'pswd') {
					http(4001);
					sqlExit();
				}
				echo json_encode(['result' => $db->query("SELECT COUNT(*) AS dup FROM users WHERE " . safe($_HEADER['dup']) . " = \"" . safe($_HEADER[ucwords(strtolower(safe($_HEADER['dup'])))]) . "\";")->fetch_assoc()['dup']]);
				sqlExit();
			}
			if (isset($_HEADER['auth'])) {
				if (isset($_HEADER['pswd'])) {
					unset($secdb);
					$secdb = $db->query("SELECT pswd FROM users WHERE username = \"" . safe($_HEADER['auth']) . "\";")->fetch_assoc()['pswd'];
					if (password_verify($_HEADER['pswd'],$secdb)) {
						do {
							unset($token);
							$token = secret();
							$tokendb = password_hash($token, PASSWORD_BCRYPT);
						} while (verify($token,false));
						if ($db->query("UPDATE users SET token = \"" . $tokendb . "\" WHERE username = \"" . safe($_HEADER['auth']) . "\";")) {
							updateSession($db->query("SELECT uid FROM users WHERE username = \"" . safe($_HEADER['auth']) . "\";")->fetch_assoc()['uid']);
							http(201);
							echo json_encode(['token' => $token]);
							sqlExit();
						}
						else mysqli_echo_error();
					}
					else {
						http(400);
						die('Authentication error.');
						sqlExit();
					}
				}
				else {
					http(4001);
					sqlExit();
				}
			}
			if (isset($_HEADER['get'])) {
				if ($tknuid) {
					switch ($_HEADER['get']) {
						case 'uid':
							echo json_encode(['uid' => $db->query("SELECT uid FROM users WHERE uid = \"" . $tknuid . "\";")->fetch_assoc()['uid']]);
							sqlExit();
							break;
						case 'email':
							echo json_encode(['email' => $db->query("SELECT email FROM users WHERE uid = \"" . $tknuid . "\";")->fetch_assoc()['email']]);
							sqlExit();
							break;
						case 'info':
							if (isset($_HEADER['uid'])) {
								if ($result = $db->query("SELECT cate FROM `" . $tknuid . "` WHERE uid = \"" . safe($_HEADER['uid']) . "\";")->fetch_assoc()['cate']) {
									echo json_encode($db->query("SELECT * FROM " . (($result == 1)?'sensors':'tables') . " WHERE uid = \"" . safe($_HEADER['uid']) . "\";")->fetch_assoc());
									sqlExit();
								}
								else {
									http(401);
									sqlExit();
								}
							}
							else {
								http(4001);
								sqlExit();
							}
							break;
						case 'sensors':
							unset($list);
							if ($result = $db->query("SELECT uid FROM `" . $tknuid . "` WHERE cate = 1;")) {
								while ($i = $result->fetch_assoc()) $list[] = $i;
								echo json_encode($list);
								sqlExit();
							}
							else mysqli_echo_error();
							break;
						case 'sensor':
							if (isset($_HEADER['uid'])) {
								if ($db->query("SELECT uid FROM `" . $tknuid . "` WHERE uid = \"" . safe($_HEADER['uid']) . "\";")->fetch_assoc()) {
									if ($db->query("SELECT cate FROM `" . $tknuid . "` WHERE uid = \"" . safe($_HEADER['uid']) . "\";")->fetch_assoc()['cate'] == 1) {
										unset($list);
										if ($result = $db->query("SELECT * FROM `" . safe($_HEADER['uid']) . "`;")) {
											while ($i = $result->fetch_assoc()) $list[] = $i;
											echo json_encode($list);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									else {
										http(4001);
										sqlExit();
									}
								}
								else {
									http(401);
									sqlExit();
								}
							}
							else {
								http(4001);
								sqlExit();
							}
							break;
						case 'tables':
							unset($list);
							if ($result = $db->query("SELECT uid FROM `" . $tknuid . "` WHERE cate = 2;")) {
								while ($i = $result->fetch_assoc()) $list[] = $i;
								echo json_encode($list);
								sqlExit();
							}
							else mysqli_echo_error();
							break;
						case 'table':
							if (isset($_HEADER['uid'])) {
								if ($db->query("SELECT uid FROM `" . $tknuid . "` WHERE uid = \"" . safe($_HEADER['uid']) . "\";")->fetch_assoc()) {
									if ($db->query("SELECT cate FROM `" . $tknuid . "` WHERE uid = \"" . safe($_HEADER['uid']) . "\";")->fetch_assoc()['cate'] == 2) {
										unset($list);
										if ($result = $db->query("SELECT * FROM `" . safe($_HEADER['uid']) . "`;")) {
											while ($i = $result->fetch_assoc()) {
												$i['name'] = $db->query("SELECT name FROM sensors WHERE uid = \"" . $i['uid'] . "\";")->fetch_assoc()['name'];
												$i['description'] = $db->query("SELECT description FROM sensors WHERE uid = \"" . $i['uid'] . "\";")->fetch_assoc()['description'];
												$i['type'] = $db->query("SELECT type FROM sensors WHERE uid = \"" . $i['uid'] . "\";")->fetch_assoc()['type'];
												unset($tmp);
												$tmp = $db->query("SELECT * FROM `" . $i['uid'] . "`;");
												while ($j = $tmp->fetch_assoc()) $data[] = $j;
												$i['data'] = $data;
												$list[] = $i;
											}
											echo json_encode($list);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									else {
										http(4001);
										sqlExit();
									}
								}
								else {
									http(401);
									sqlExit();
								}
							}
							else {
								http(4001);
								sqlExit();
							}
							break;
						default:
							http(4001);
							sqlExit();
							break;
					}
				}
				else{
					http(401);
					sqlExit();
				}
			}
			http(405);
			sqlExit();
			break;
		case 'POST':
			if (!$tknuid) {
				if (isset($_HTTP['cate']) && $_HTTP['cate'] == 'user') {
					if (isset($_HTTP['username']) && $_HTTP['username'] && isset($_HTTP['pswd']) && isset($_HTTP['email']) && filter_var(safe($_HTTP['email']),FILTER_VALIDATE_EMAIL)) {
						if (!($db->query("SELECT uid FROM users WHERE username = \"" . safe($_HTTP['username']) . "\";")->fetch_assoc())) {
							if (!($db->query("SELECT uid FROM users WHERE email = \"" . safe($_HTTP['email']) . "\";")->fetch_assoc())) {
								unset($uid);
								$uid = preg_replace("/[^A-Za-z0-9 ]/", '', uniqid('', true));
								unset($pswddb);
								$pswddb = password_hash($_HTTP['pswd'], PASSWORD_BCRYPT);
								do {
									unset($token);
									$token = secret();
									$tokendb = password_hash($token, PASSWORD_BCRYPT);
								} while (verify($token,false));
								if ($db->query("INSERT INTO users (uid, username, pswd, token, email) VALUES (\"" . $uid . "\", \"" . safe($_HTTP['username']) . "\", \"" . $pswddb . "\", \"" . $tokendb . "\", \"" . filter_var(safe($_HTTP['email']),FILTER_VALIDATE_EMAIL) . "\");")) {
									if ($db->query("CREATE TABLE " . $uid . " (uid TINYTEXT NOT NULL, cate TINYINT NOT NULL, UNIQUE uid (uid(32))) ENGINE = InnoDB;")) {
										http(201);
										echo json_encode(['uid' => $uid]);
										sqlExit();
									}
									else mysqli_echo_error();
								}
								else mysqli_echo_error();
							}
							else {
								http(400);
								die('That email is taken.');
								sqlExit();
							}
						}
						else {
							http(400);
							die('That username is taken.');
							sqlExit();
						}
					}
					else {
						http(4001);
						sqlExit();
					}
				}
				else{
					http(401);
					sqlExit();
				}
			}
			elseif (isset($_HTTP['cate'])) {
				switch ($_HTTP['cate']) {
					case 'sensor':
						if (isset($_HTTP['name']) && $_HTTP['name'] && isset($_HTTP['desc']) && $_HTTP['desc'] && isset($_HTTP['type']) && filter_var(safe($_HTTP['type']),FILTER_VALIDATE_INT)) {
							unset($uid);
							$uid = preg_replace("/[^A-Za-z0-9 ]/", '', uniqid('', true));
							unset($type);
							switch (filter_var(safe($_HTTP['type']),FILTER_VALIDATE_INT)) {
								case 1:
									$type = 'TINYINT';
									break;
								case 2:
									$type = 'SMALLINT';
									break;
								case 3:
									$type = 'MEDIUMINT';
									break;
								case 4:
									$type = 'INT';
									break;
								case 5:
									$type = 'BIGINT';
									break;
								case 6:
									$type = 'DECIMAL(65,30)';
									break;
								case 7:
									$type = 'FLOAT';
									break;
								case 8:
									$type = 'DOUBLE';
									break;
								case 11:
									$type = 'TINYINT UNSIGNED';
									break;
								case 12:
									$type = 'SMALLINT UNSIGNED';
									break;
								case 13:
									$type = 'MEDIUMINT UNSIGNED';
									break;
								case 14:
									$type = 'INT UNSIGNED';
									break;
								case 15:
									$type = 'BIGINT UNSIGNED';
									break;
								case 16:
									$type = 'DECIMAL UNSIGNED';
									break;
								case 17:
									$type = 'FLOAT UNSIGNED';
									break;
								case 18:
									$type = 'DOUBLE UNSIGNED';
									break;
								case 21:
									$type = 'TINYTEXT';
									break;
								case 22:
									$type = 'TEXT';
									break;
								case 23:
									$type = 'MEDIUMTEXT';
									break;
								case 24:
									$type = 'LONGTEXT';
									break;
								default:
									http(4001);
									sqlExit();
									break;
							}
							if ($db->query("INSERT INTO sensors (uid, owner, name, description, type) VALUES (\"" . $uid . "\", \"" . $tknuid . "\", \"" . safe($_HTTP['name']) . "\", \"" . safe($_HTTP['desc']) . "\", \"" . filter_var(safe($_HTTP['type']),FILTER_VALIDATE_INT) . "\");")) {
								if ($db->query("CREATE TABLE " . $uid . " (createTime BIGINT NOT NULL, val " . $type . " NOT NULL) ENGINE = InnoDB;") && $db->query("INSERT INTO `" . $tknuid . "` (uid, cate) VALUES (\"" . $uid . "\", 1);")) {
									http(201);
									echo json_encode(['uid' => $uid, 'type' => $type]);
									sqlExit();
								}
								else mysqli_echo_error();
							}
							else mysqli_echo_error();
						}
						else {
							http(4001);
							sqlExit();
						}
						break;
					case 'table':
						if (isset($_HTTP['name']) && $_HTTP['name'] && isset($_HTTP['desc']) && $_HTTP['desc']) {
							unset($uid);
							$uid = preg_replace("/[^A-Za-z0-9 ]/", '', uniqid('', true));
							if ($db->query("INSERT INTO tables (uid, owner, name, description) VALUES (\"" . $uid . "\", \"" . $tknuid . "\", \"" . safe($_HTTP['name']) . "\", \"" . safe($_HTTP['desc']) . "\");")) {
								if ($db->query("CREATE TABLE " . $uid . " (uid TINYTEXT NOT NULL, layout TINYINT NOT NULL, UNIQUE uid (uid(32))) ENGINE = InnoDB;") && $db->query("INSERT INTO `" . $tknuid . "` (uid, cate) VALUES (\"" . $uid . "\", 2);")) {
									http(201);
									echo json_encode(['uid' => $uid]);
									sqlExit();
								}
								else mysqli_echo_error();
							}
							else mysqli_echo_error();
						}
						else {
							http(4001);
							sqlExit();
						}
						break;
					default:
						http(4001);
						sqlExit();
						break;
				}
			}
			http(405);
			sqlExit();
			break;
		case 'PUT':
			if ($tknuid) {
				if (isset($_HTTP['cate'])) {
					unset($putData);
					$putData = '';
					switch ($_HTTP['cate']) {
						case 'user':
							if (isset($_HTTP['username'])) {
								if (!($db->query("SELECT uid FROM users WHERE username = \"" . safe($_HTTP['username']) . "\";")->fetch_assoc())) {
									$putData .= ($putData)?',':'';
									$putData .= 'username = "' . safe($_HTTP['username']) . '"';
								}
								else {
									http(400);
									die('That username is taken.');
									sqlExit();
								}
							}
							if (isset($_HTTP['email'])) {
								if (filter_var(safe($_HTTP['email']),FILTER_VALIDATE_EMAIL)) {
									if (!($db->query("SELECT uid FROM users WHERE email = \"" . filter_var(safe($_HTTP['email']),FILTER_VALIDATE_EMAIL) . "\";")->fetch_assoc())) {
										$putData .= ($putData)?',':'';
										$putData .= 'email = "' . safe($_HTTP['email']) . '"';
									}
									else {
										http(400);
										die('That email is taken.');
										sqlExit();
									}
								}
								else {
									http(4001);
									sqlExit();
								}
							}
							if (isset($_HTTP['pswd'])) {
								unset($pswddb);
								$pswddb = password_hash($_HTTP['pswd'], PASSWORD_BCRYPT);
								$putData .= ($putData)?',':'';
								$putData .= 'pswd = "' . $pswddb . '"';
							}
							if ($db->query("UPDATE users SET " . $putData . " WHERE uid = \"" . $tknuid . "\";")) {
								http(204);
								sqlExit();
							}
							else mysqli_echo_error();
							break;
						case 'sensor':
							if (isset($_HTTP['uid'])) {
								if ($db->query("SELECT uid FROM `" . $tknuid . "` WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()) {
									if (isset($_HTTP['val'])) {
										if ($db->query("INSERT INTO `" . safe($_HTTP['uid']) . "` (createTime, val) VALUES (" . strtotime('now') . ", \"" . safe($_HTTP['val']) . "\");")) {
											http(204);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									elseif ($db->query("SELECT owner FROM sensors WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()['owner'] == $tknuid) {
										if (isset($_HTTP['user']) && $db->query("SELECT uid FROM users WHERE uid = \"" . safe($_HTTP['user']) . "\";")->fetch_assoc()) {
											if ($db->query("INSERT INTO `" . safe($_HTTP['user']) . "` (uid, cate) VALUES (\"" . safe($_HTTP['uid']) . "\", 1);") && $db->query("UPDATE sensors SET users = \"" . (($db->query("SELECT users FROM sensors WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()['users'])?(($db->query("SELECT users FROM sensors WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()['users']) . ','):'') . safe($_HTTP['user']) . "\" WHERE uid = \"" . safe($_HTTP['uid']) . "\";")) {
												http(204);
												sqlExit();
											}
											else mysqli_echo_error();
										}
										elseif (isset($_HTTP['name'])) {
											if ($db->query("UPDATE sensors SET name = \"" . safe($_HTTP['name']) . "\" WHERE uid = \"" . safe($_HTTP['uid']) . "\";")) {
												http(204);
												sqlExit();
											}
											else mysqli_echo_error();
										}
										elseif (isset($_HTTP['desc'])) {
											if ($db->query("UPDATE sensors SET description = \"" . safe($_HTTP['desc']) . "\" WHERE uid = \"" . safe($_HTTP['uid']) . "\";")) {
												http(204);
												sqlExit();
											}
											else mysqli_echo_error();
										}
										else {
											http(4001);
											sqlExit();
										}
									}
									else {
										http(401);
										sqlExit();
									}
								}
								else {
									http(401);
									sqlExit();
								}
							}
							else {
								http(4001);
								sqlExit();
							}
							break;
						case 'table':
							if (isset($_HTTP['uid'])) {
								if ($db->query("SELECT owner FROM tables WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()['owner'] == $tknuid) {
									if (isset($_HTTP['sensor']) && $db->query("SELECT uid FROM sensors WHERE uid = \"" . safe($_HTTP['sensor']) . "\";")->fetch_assoc()) {
										if ($db->query("INSERT INTO `" . safe($_HTTP['uid']) . "` (sensor, layout) VALUES (\"" . safe($_HTTP['sensor']) . "\", " . safe($_HTTP['layout']) . ");") && $db->query("UPDATE sensors SET tables = \"" . (($db->query("SELECT tables FROM sensors WHERE uid = \"" . safe($_HTTP['sensor']) . "\";")->fetch_assoc()['tables'])?(($db->query("SELECT tables FROM sensors WHERE uid = \"" . safe($_HTTP['sensor']) . "\";")->fetch_assoc()['tables']) . ','):'') . safe($_HTTP['uid']) . "\" WHERE uid = \"" . safe($_HTTP['sensor']) . "\";")) {
											http(204);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									elseif (isset($_HTTP['user']) && $db->query("SELECT uid FROM users WHERE uid = \"" . safe($_HTTP['user']) . "\";")->fetch_assoc()) {
										if ($db->query("INSERT INTO `" . safe($_HTTP['user']) . "` (uid, cate) VALUES (\"" . safe($_HTTP['uid']) . "\", 2);") && $db->query("UPDATE tables SET users = \"" . (($db->query("SELECT users FROM tables WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()['users'])?(($db->query("SELECT users FROM tables WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()['users']) . ','):'	') . safe($_HTTP['user']) . "\" WHERE uid = \"" . safe($_HTTP['uid']) . "\";")) {
											http(204);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									elseif (isset($_HTTP['name'])) {
										if ($db->query("UPDATE tables SET name = \"" . safe($_HTTP['name']) . "\" WHERE uid = \"" . safe($_HTTP['uid']) . "\";")) {
											http(204);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									elseif (isset($_HTTP['desc'])) {
										if ($db->query("UPDATE tables SET description = \"" . safe($_HTTP['desc']) . "\" WHERE uid = \"" . safe($_HTTP['uid']) . "\";")) {
											http(204);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									else {
										http(4001);
										sqlExit();
									}
								}
								else {
									http(401);
									sqlExit();
								}
							}
							else {
								http(4001);
								sqlExit();
							}
							break;
						default:
							http(4001);
							sqlExit();
							break;
					}
				}
				else{
					http(405);
					sqlExit();
				}
			}
			http(401);
			sqlExit();
			break;
		case 'DELETE':
			if ($tknuid) {
				if (isset($_HTTP['cate'])) {
					switch ($_HTTP['cate']) {
						case 'token':
							if ($db->query("UPDATE users SET session = 0 WHERE uid = \"" . $tknuid . "\";")) http(204);
							else mysqli_echo_error();
							break;
						case 'sensor':
							if (isset($_HTTP['uid']) && $db->query("SELECT uid FROM sensors WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()) {
								if ($db->query("SELECT owner FROM sensors WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()['owner'] == $tknuid) {
									if ($result = $db->query("SELECT owner,users,tables FROM sensors WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()) {
										unset($list);
										$list = $result['owner'] . ',' . $result['users'] . ',' . $result['tables'];
										del(safe($_HTTP['uid']), $list);
										if ($db->query("DELETE FROM sensors WHERE uid = \"" . safe($_HTTP['uid']) . "\";") && $db->query("DROP TABLE `" . safe($_HTTP['uid']) . "`;")) {
											http(204);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									else mysqli_echo_error();
								}
								else {
									http(401);
									sqlExit();
								}
							}
							elseif (isset($_HTTP['remove']) && $db->query("SELECT uid FROM sensors WHERE uid = \"" . safe($_HTTP['from']) . "\";")->fetch_assoc()) {
								if ($db->query("SELECT owner FROM sensors WHERE uid = \"" . safe($_HTTP['from']) . "\";")->fetch_assoc()['owner'] == $tknuid) {
									if ($db->query("DELETE FROM `" . safe($_HTTP['remove']) . "` WHERE uid = \"" . safe($_HTTP['from']) . "\";")) {
										http(204);
										sqlExit();
									}
									else mysqli_echo_error();
								}
								else {
									http(401);
									sqlExit();
								}
							}
							else {
								http(4001);
								sqlExit();
							}
							break;
						case 'table':
							if (isset($_HTTP['from']) && $db->query("SELECT uid FROM tables WHERE uid = \"" . safe($_HTTP['from']) . "\";")->fetch_assoc()) {
								if ($db->query("SELECT owner FROM tables WHERE uid = \"" . safe($_HTTP['from']) . "\";")->fetch_assoc()['owner'] == $tknuid) {
									if (isset($_HTTP['uid'])) {
										if ($db->query("DELETE FROM `" . safe($_HTTP['from']) . "` WHERE uid = \"" . safe($_HTTP['uid']) . "\";")) {
											http(204);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									elseif (isset($_HTTP['remove'])) {
										if ($db->query("DELETE FROM `" . safe($_HTTP['remove']) . "` WHERE uid = \"" . safe($_HTTP['from']) . "\";")) {
											http(204);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									else {
										http(4001);
										sqlExit();
									}
								}
								else {
									http(401);
									sqlExit();
								}
							}
							elseif (isset($_HTTP['uid']) && $db->query("SELECT uid FROM tables WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()) {
								if ($db->query("SELECT owner FROM tables WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()['owner'] == $tknuid) {
									if ($result = $db->query("SELECT owner,users FROM tables WHERE uid = \"" . safe($_HTTP['uid']) . "\";")->fetch_assoc()) {
										unset($list);
										$list = $result['owner'] . ',' . $result['users'];
										del(safe($_HTTP['uid']), $list);
										if ($db->query("DELETE FROM tables WHERE uid = \"" . safe($_HTTP['uid']) . "\";") && $db->query("DROP TABLE `" . safe($_HTTP['uid']) . "`;")) {
											http(204);
											sqlExit();
										}
										else mysqli_echo_error();
									}
									else mysqli_echo_error();
								}
								else {
									http(401);
									sqlExit();
								}
							}
							else {
								http(4001);
								sqlExit();
							}
							break;
						case 'user':
							if (isset($_HTTP['uid']) && isset($_HTTP['pswd'])) {
								unset($secdb);
								$secdb = $db->query("SELECT pswd FROM users WHERE username = \"" . $tknuid . "\";")->fetch_assoc()['pswd'];
								if ($_HTTP['uid'] == $tknuid && password_verify($_HTTP['pswd'],$secdb)) {
									unset($sensors);
									unset($tables);
									$result = $db->query("SELECT uid,users,tables FROM sensors WHERE owner = \"" . $tknuid . "\";");
									while ($i = $result->fetch_assoc()) {
										$i['del'] = $i['users'];
										unset($i['users']);
										$i['del'] .= ($i['del'])?(',' . $i['tables']):$i['tables'];
										unset($i['tables']);
										$sensors[] = $i;
									}
									batchDel($sensors);
									$result = $db->query("SELECT uid,users AS del FROM tables WHERE owner = \"" . $tknuid . "\";");
									while ($i = $result->fetch_assoc()) $tables[] = $i;
									batchDel($tables);
									if ($db->query("DELETE FROM users WHERE uid = \"" . $tknuid . "\";") && $db->query("DROP TABLE `" . safe($_HTTP['uid']) . "`;")) {
										http(204);
										sqlExit();
									}
									else mysqli_echo_error();
								}
								else {
									http(400);
									die('Authentication error.');
									sqlExit();
								}
							}
							else {
								http(4001);
								sqlExit();
							}
							break;
						default:
							http(4001);
							sqlExit();
							break;
					}
				}
				else{
					http(405);
					sqlExit();
				}
			}
			http(401);
			sqlExit();
			break;
		default:
			http(405);
			sqlExit();
			break;
	}
